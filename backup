#!/bin/bash

#
# Author: Kurt L Vanderwater
# Date:   October 2, 2001
# Purpose:
#	This program is used to backup all "user" directories
#
# Updates:
#	Jan 14, 2002 - klv - Added logic for removing runtime from age equation
#	Dec 29, 2001 - klv - Changed from single file megabackup to directory based archives
#	Dec 29, 2001 - klv - commented out deletion of tempory executable
#	Dec 29, 2001 - klv - moved all variables to top of program for easy maint
#


#################################
### VARIABLES 
#################################

#################################
# yesterday
#   This variable is used to define a folder name.
#   It is used when we are creating the next 'current' folder.
#   So, current becomes the date of 'yesterday'
#
yesterday=$(date --date="yesterday" +"%Y-%m-%d")


#################################
# bdays
#   This is the number of days back (versions) that we want to keep on disk.
bdays=1


#################################
# backup_eldest
#   The date format for the number of days back (versions) that we want to keep on disk.
#
backup_eldest=$(date --date="-${bdays} days" +%Y%b%d)


#################################
# mdays
#   This is the number of days back (versions) that we want to keep on disk.
mdays=5


#################################
# mysql_eldest
#   The date format for the number of days back (versions) that we want to keep on disk.
#
mysql_eldest=$(date --date="-${mdays} days" +"%Y-%m-%d")


#################################
# DIRNAME
#   This is the foldername that is used for the data
#
DIRNAME=$(date +%Y%b%d)		# datestamp name for directory


#################################
# TMPFILE1
# TMPAWK1
#   These files are used during the build process for 'home' folder backups
#
TMPFILE1="/tmp/backup_1"	# temporary file used to build the backup script
TMPFILE2="/tmp/backup_2"	# temporary file used to build the backup script
TMPAWK1="/tmp/backup_awk_1"	# temporary file used to define the AWK program to build the backup script


#################################
# BDIR
#  This is the base folder where daily backups are kept. 
#
BDIR="/var/spool/backup/daily"	# base directory for the destination of all the backups.


#################################
# MDIR
#  This is the base folder where daily mysql dumps are kept.
#
MDIR="/var/spool/mysqldumps"    # base directory for the destination of all mysql dumps.


#################################
# The 3 lists below are NameLIST, DirectoryLIST, & ExcludeLIST. These are used to define the name of 
# the backup file, the base directory for the backup file, and any exclusions that should occur from that
# directory structure.
#
i=0
NLIST[$i]="bak_etc";		DLIST[$i]="/etc";	ELIST[$((i++))]="";
NLIST[$i]="bak_var";		DLIST[$i]="/var";	ELIST[$((i++))]="spool/backup --exclude=spool/mysqldumps --exclude=cache --exclude=log --exclude=tmp --exclude=www --exclude=run --exclude=lock";
NLIST[$i]="bak_var_www";	DLIST[$i]="/var/www";	ELIST[$((i++))]="";
NLIST[$i]="bak_usr";		DLIST[$i]="/usr";	ELIST[$((i++))]="";
NLIST[$i]="bak_root";		DLIST[$i]="/root";	ELIST[$((i++))]="";

#################################
# Defining scriptbypasswd
#   This function looks for users by parsing the passwd file in /etc
function scriptbypasswd() {
	# build the script to actually do the backups

	cat <<"EOF" | sed -e "s~Z-0-Z~$DIRNAME~" | sed -e "s~Z-1-Z~$BDIR~" > $TMPAWK1
	BEGIN { 
		FS = ":"
	}
	{
		if($3 >= 500 && $3 < 65000){
			if($1 == "webtrends"){
				;
			} else {
				print "cd "$6
				print "rc=$?"
				print "if (($rc == 0)); then"
				print " tar -czf Z-1-Z/Z-0-Z/"$1".tar.gz *"
				print " chmod 660 Z-1-Z/Z-0-Z/"$1".tar.gz"
				print " if [ '"$7"' = '/bin/true' ]; then"
				print "  chown "$1" Z-1-Z/Z-0-Z/"$1".tar.gz"
				print "  chgrp webadmin Z-1-Z/Z-0-Z/"$1".tar.gz"
				print " else"
				print "  chown "$1" Z-1-Z/Z-0-Z/"$1".tar.gz"
				print " fi"
				print " rc=$?"
				print " if (($rc == 0)); then"
				print " 	rm -f "$6"/backup.tar.gz"
				print " 	ln -s Z-1-Z/Z-0-Z/"$1".tar.gz "$6"/backup.tar.gz"
				print " 	chown "$1" "$6"/backup.tar.gz"
				print " fi"
				print "fi"
			}
		}
	}
EOF

	cat /etc/passwd | awk -f $TMPAWK1 > $TMPFILE1

}

#################################
# Defining scriptbyhome
#   This function looks for users by folders in /home
function scriptbyhome() {
	# build the script to actually do the backups

	cat <<"EOF" | sed -e "s~Z-0-Z~$DIRNAME~" | sed -e "s~Z-1-Z~$BDIR~" > $TMPAWK1
	BEGIN { 
		FS = ":"
	}
	{
		print "cd /home/"$1
		print "rc=$?"
		print "if (($rc == 0)); then"
		print " tar -czf Z-1-Z/Z-0-Z/"$1".tar.gz *"
		print " chmod 660 Z-1-Z/Z-0-Z/"$1".tar.gz"
		print " chown "$1" Z-1-Z/Z-0-Z/"$1".tar.gz"
		print " rc=$?"
		print " if (($rc == 0)); then"
		print " 	rm -f "$1"/backup.tar.gz"
		print " 	ln -s Z-1-Z/Z-0-Z/"$1".tar.gz "$1"/backup.tar.gz"
		print " 	chown "$1" "$1"/backup.tar.gz"
		print " fi"
		print "fi"
	}
EOF

	ls -al /home | grep ^d | sed '1,2d' | awk '{print $9}' | awk -f $TMPAWK1 > $TMPFILE1

}

function mysql_back() {
#################################
# Set folder for current Mysql Databases
#   The 'find' command should be updated before year 2100
if [ ! -d $MDIR ]; then
	mkdir -p $MDIR;
fi

pushd $MDIR
mv current $yesterday
rm -fR $mysql_eldest
find 20* -maxdepth 1 -type d -ctime +${mdays} -exec rm -fR {} \;
popd
#################################
# Backup the Mysql Databases
#   This requires that a small script be built to pass into from xargs
cat <<"EOF" | sed -e "s~Z-0-Z~$MDIR~" > $TMPFILE2
#!/bin/bash

# Programmer: Kurt L Vanderwater
# Written:    10-4-11
# Purpose:
#     Manage backups of the mysql databases
#
# Params:
#   $1 - The database name

mkdir -p Z-0-Z/current
pushd Z-0-Z/current

echo "Processing Database $1"
mysqldump --skip-extended-insert $1 | gzip > ${1}.sql.gz
ls -l ${1}.sql.gz

popd
EOF

chmod a+x $TMPFILE2
mysql -Bse "show databases" | grep -v "mysql\|information_schema\|lost+found" | xargs -I{} $TMPFILE2 {}
}

function backup_main() {
#################################
# Check and see if the backup directory exists
if [ ! -d $BDIR/$DIRNAME ]; then
	mkdir -p $BDIR/$DIRNAME;
fi

#################################
# save our directory position
pushd . > /dev/null 2>&1

#################################
# get rid of any old backups
#   The 'find' command should be updated before year 2100
pushd . > /dev/null 2>&1
cd $BDIR
rm -fR $backup_eldest
find 20* -maxdepth 1 -type d -ctime +${bdays} -exec rm -fR {} \;
popd


#################################
# backup the specified directories now
for (( i=0; i<$((${#DLIST[@]})); i++ )); do
	pushd . > /dev/null 2>&1
	cd ${DLIST[i]}
  if [[ -n ${ELIST[i]} ]]; then
    tar -czf $BDIR/$DIRNAME/${NLIST[i]}.tar.gz --exclude=${ELIST[i]} *;
  else
    tar -czf $BDIR/$DIRNAME/${NLIST[i]}.tar.gz *;
  fi
	popd
done

#################################
# do the client backups now
# There are 2 different methods that you can use to backup client folders.
# The first parses the passwd file, and finds all home folders belonging to 
# UID numbers > 500.
# The second just uses /home as its base and works from there.
# You only want to pick ONE of these to execute
scriptbypasswd
# scriptbyhome

pushd . > /dev/null 2>&1
. $TMPFILE1
popd

#################################
# return the directory to normal

popd

rm -f $TMPFILE1
}
mysql_back
# backup_main
